﻿using IdentityServerApp.Data;
using IdentityServerApp.Models.Notes;
using Microsoft.EntityFrameworkCore;

namespace IdentityServerApp.Managers;

public class NoteManager
{
    private readonly AppDbContext _context;

    public NoteManager(AppDbContext context)
    {
        _context = context;
    }

    public async Task<Note> AddNoteAsync(Note note)
    {
        _context.Notes.Add(note);
        await _context.SaveChangesAsync();
        return note;
    }

    public async Task<List<ShowNote>> GetUserNotesAsync(string userId)
    {
        return await _context.Notes
            .Where(note => note.AuthorId == userId)
            .Select(note => new ShowNote
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                CreatedAt = note.CreatedAt,
                IsPrivate =note.IsPrivate,
                IsSuggest = note.Suggested
            })
            .ToListAsync();
    }
    public async Task<List<ShowNote>> GetSuggestedUserNotesAsync(string userId)
    {
        return await _context.Notes
            .Where(note => note.AuthorId == userId && note.Suggested == true)
            .Select(note => new ShowNote
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                CreatedAt = note.CreatedAt,
                IsPrivate =note.IsPrivate,
                IsSuggest = note.Suggested
            })
            .ToListAsync();
    }
    public async Task<List<ShowNote>> GetPublicNotesAsync(string userId, bool isAdmin)
    {
        return await _context.Notes
            .Where(note => note.AuthorId == userId && (isAdmin || !note.IsPrivate) && !note.Suggested)
            .Select(note => new ShowNote
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                CreatedAt = note.CreatedAt,
                IsPrivate =note.IsPrivate,
                IsSuggest = note.Suggested
            })
            .ToListAsync();
    }

    public async Task<UpdateNote> UpdateNoteAsync(Note note)
    {
        _context.Notes.Update(note);
        await _context.SaveChangesAsync();

        // Создаем и возвращаем объект UpdateNote на основе обновленной заметки
        return new UpdateNote
        {
            Id = note.Id,
            Title = note.Title,
            Content = note.Content,
            CreatedAt = note.CreatedAt,
            IsPrivate = note.IsPrivate
        };
    }

    public async Task<bool> DeleteNoteAsync(int noteId)
    {
        var note = await _context.Notes.FindAsync(noteId);
        if (note == null)
        {
            return false;
        }

        _context.Notes.Remove(note);
        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<Note> GetNoteByIdAsync(int id)
    {
        return await _context.Notes.FindAsync(id);
    }
    
    public async Task<bool> IsUserNoteOwnerAsync(string userId, int noteId)
    {
        return await _context.Notes
            .AnyAsync(note => note.Id == noteId && note.AuthorId == userId);
    }
}