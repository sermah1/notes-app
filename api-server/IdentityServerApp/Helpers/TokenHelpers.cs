﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace IdentityServerApp.Helpers;

public static class TokenHelpers
{
    public static string ValidateAndExtractUserId(string token, string secretKey)
    {
        var tokenHandler = new JwtSecurityTokenHandler();

        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)),
            ValidateIssuer = false,
            ValidateAudience = false,
            ClockSkew = TimeSpan.Zero
        };

        var claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out var validatedToken);

        if (!(validatedToken is JwtSecurityToken jwtSecurityToken) ||
            !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                StringComparison.InvariantCultureIgnoreCase))
        {
            throw new SecurityTokenException("Invalid token");
        }

        var userIdClaim = claimsPrincipal.Claims.First(claim => claim.Type == ClaimTypes.Sid);
        return userIdClaim.Value;
    }
}