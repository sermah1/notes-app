﻿using Microsoft.AspNetCore.Identity;

namespace IdentityServerApp.Models.Notes;

public class Note
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime CreatedAt { get; set; }
    
    public string AuthorId { get; set; } 
    public bool IsPrivate { get; set; }
    public bool Suggested { get; set; }
    public IdentityUser User { get; set; }
}
