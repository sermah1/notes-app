﻿using Microsoft.AspNetCore.Identity;

namespace IdentityServerApp.Models.Notes;

public class AddNote
{
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime CreatedAt { get; set; }
    public bool IsPrivate { get; set; }
    public IdentityUser? User { get; set; }
}