namespace IdentityServerApp.Models.Notes;

public class ApproveNote
{
    public int Id { get; set; }
    public bool IsApprove { get; set; }
}