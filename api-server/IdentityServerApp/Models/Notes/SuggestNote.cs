using Microsoft.AspNetCore.Identity;

namespace IdentityServerApp.Models.Notes;

public class SuggestNote
{
    public string UserId { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime CreatedAt { get; set; }
}