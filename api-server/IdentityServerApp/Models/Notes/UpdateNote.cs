﻿using Microsoft.AspNetCore.Identity;

namespace IdentityServerApp.Models.Notes;

public class UpdateNote
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Content { get; set; }
    public DateTime? CreatedAt { get; set; }
    public bool? IsPrivate { get; set; }
}