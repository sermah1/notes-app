using IdentityServerApp.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServerApp.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly IConfiguration _configuration;
    private readonly string _secretKey;

    public UserController(
        IConfiguration configuration, UserManager<IdentityUser> userManager)
    {
        _userManager = userManager;
        _configuration = configuration;
        _secretKey = _configuration["Jwt:Key"]!;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserById(string id)
    {
        try
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                var result = new { id = user.Id, username = user.UserName };
                return Ok(result);
            }

            return NotFound("User not found");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpGet("by-username/{username}")]
    public async Task<IActionResult> GetUserByUsername(string username)
    {
        try
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user != null)
            {
                var result = new { id = user.Id, username = user.UserName };
                return Ok(result);
            }

            return NotFound("User not found");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}