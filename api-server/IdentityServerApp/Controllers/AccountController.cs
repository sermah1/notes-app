﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using IdentityServerApp.Helpers;
using IdentityServerApp.Managers;
using IdentityServerApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace IdentityServerApp.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IConfiguration _configuration;
    private readonly string _secretKey;

    public AccountController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager,
        IConfiguration configuration, NoteManager noteManager)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _configuration = configuration;
        _secretKey = _configuration["Jwt:Key"]!;
    }



    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] Register model)
    {
        try
        {
            var user = new IdentityUser { UserName = model.Username };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.SetEmailAsync(user, model.Email);
                return Ok(new { message = "User registered successfully" });
            }

            return BadRequest(result.Errors);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] Login model)
    {
        try
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>()
                {
                    new(JwtRegisteredClaimNames.Sub, user.UserName!),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new(ClaimTypes.Sid, user.Id),
                };
                authClaims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));
                var token = new JwtSecurityToken(
                    issuer: _configuration["Jwt:Issuer"],
                    expires: DateTime.Now.AddMinutes(double.Parse(_configuration["Jwt:ExpiryMinutes"]!)),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_secretKey)),
                        SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    id = user.Id,
                    username = user.UserName,
                    email = user.Email,
                    userRoles = userRoles,
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                });
            }

            return Unauthorized();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("auth")]
    public async Task<IActionResult> Auth([FromBody] Auth model)
    {
        try
        {
            var userId = TokenHelpers.ValidateAndExtractUserId(model.Token, _secretKey);

            var user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                var username = await _userManager.GetUserNameAsync(user);
                var email = await _userManager.GetEmailAsync(user);
                var role = await _userManager.GetRolesAsync(user);
                return Ok(new
                {
                    id = userId,
                    username,
                    email,
                    role,
                    model.Token
                });
            }

            return BadRequest($"User with id {userId} not found");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("add-role")]
    public async Task<IActionResult> AddRole([FromBody] string role)
    {
        try
        {
            if (!await _roleManager.RoleExistsAsync(role))
            {
                var result = await _roleManager.CreateAsync(new IdentityRole(role));
                if (result.Succeeded)
                {
                    return Ok(new { message = "Role added successfully" });
                }

                return BadRequest(result.Errors);
            }

            return BadRequest("Role alreay exists");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("assign-role")]
    public async Task<IActionResult> AssignRole([FromBody] UserRole model)
    {
        try
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                return BadRequest("User not found");
            }

            var result = await _userManager.AddToRoleAsync(user, model.Role);
            if (result.Succeeded)
            {
                return Ok(new { message = "Role assigned successfully" });
            }

            return BadRequest(result.Errors);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}