﻿using IdentityServerApp.Helpers;
using IdentityServerApp.Managers;
using IdentityServerApp.Models;
using IdentityServerApp.Models.Notes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServerApp.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NoteController : ControllerBase
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly NoteManager _noteManager;
    private readonly IConfiguration _configuration;
    private readonly string _secretKey;

    public NoteController(
        IConfiguration configuration, NoteManager noteManager, UserManager<IdentityUser> userManager)
    {
        _noteManager = noteManager;
        _userManager = userManager;
        _configuration = configuration;
        _secretKey = _configuration["Jwt:Key"]!;
    }

    [HttpGet("get-user-notes")]
    public async Task<IActionResult> GetUserNotes()
    {
        try
        {
            var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Token cannot be null or empty.");
            }

            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var result = await _noteManager.GetUserNotesAsync(userId);
                return Ok(result);
            }

            return BadRequest("User not found");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("get-suggested-user-notes")]
    public async Task<IActionResult> GetSuggestedUserNotes()
    {
        try
        {
            var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Token cannot be null or empty.");
            }

            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var result = await _noteManager.GetSuggestedUserNotesAsync(userId);
                return Ok(result);
            }

            return BadRequest("User not found");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("get-public-user-notes/{Id}")]
    public async Task<IActionResult> GetPublicUserNotes(string Id)
    {
        try
        {
            var requestedUserToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

            if (string.IsNullOrEmpty(requestedUserToken))
            {
                return BadRequest("Token cannot be null or empty. Use: Bearer ");
            }

            var requestedUserId = TokenHelpers.ValidateAndExtractUserId(requestedUserToken, _secretKey);
            var requestedUser = await _userManager.FindByIdAsync(requestedUserId);
            var ownerUser = await _userManager.FindByIdAsync(Id);
            if (ownerUser != null && requestedUser != null)
            {
                var role = await _userManager.GetRolesAsync(requestedUser);
                var result =
                    await _noteManager.GetPublicNotesAsync(ownerUser.Id, role.Contains("admin"));
                return Ok(result);
            }

            return BadRequest("User not found");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("add-note")]
    [Authorize]
    public async Task<IActionResult> AddNote([FromBody] AddNote model)
    {
        if (model == null)
        {
            return BadRequest("Note model cannot be null.");
        }

        var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

        if (string.IsNullOrEmpty(token))
        {
            return BadRequest("Token cannot be null or empty.");
        }

        try
        {
            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);

            var user = await _userManager.FindByIdAsync(userId);
            var note = new Note()
            {
                Title = model.Title,
                Content = model.Content,
                CreatedAt = model.CreatedAt,
                AuthorId = userId,
                IsPrivate = model.IsPrivate,
                Suggested = false,
                User = user
            };
            var result = await _noteManager.AddNoteAsync(note);

            return CreatedAtAction(nameof(GetNoteById), new { id = result.Id }, result);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("update-note")]
    [Authorize]
    public async Task<IActionResult> UpdateNote([FromBody] UpdateNote model)
    {
        if (model == null)
        {
            return BadRequest("Note model cannot be null.");
        }

        var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

        if (string.IsNullOrEmpty(token))
        {
            return BadRequest("Token cannot be null or empty.");
        }

        try
        {
            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);

            var user = await _userManager.FindByIdAsync(userId);
            if (await _noteManager.IsUserNoteOwnerAsync(userId, model.Id))
            {
                // Получаем существующую заметку из базы данных
                var existingNote = await _noteManager.GetNoteByIdAsync(model.Id);

                if (existingNote == null)
                {
                    return NotFound("Note not found");
                }

                if (model.Title != null)
                {
                    existingNote.Title = model.Title;
                }

                if (model.Content != null)
                {
                    existingNote.Content = model.Content;
                }

                if (model.CreatedAt.HasValue)
                {
                    existingNote.CreatedAt = model.CreatedAt.Value;
                }

                if (model.IsPrivate.HasValue)
                {
                    existingNote.IsPrivate = model.IsPrivate.Value;
                }

                var result = await _noteManager.UpdateNoteAsync(existingNote);

                return CreatedAtAction(nameof(GetNoteById), new { id = result.Id }, result);
            }

            return BadRequest("The user is not the owner");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("approve")]
    [Authorize]
    public async Task<IActionResult> ApproveNote([FromBody] ApproveNote model)
    {
        if (model == null)
        {
            return BadRequest("Note model cannot be null.");
        }

        var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

        if (string.IsNullOrEmpty(token))
        {
            return BadRequest("Token cannot be null or empty.");
        }

        try
        {
            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);

            var user = await _userManager.FindByIdAsync(userId);

            if (await _noteManager.IsUserNoteOwnerAsync(userId, model.Id))
            {
                var existingNote = await _noteManager.GetNoteByIdAsync(model.Id);

                if (existingNote == null)
                {
                    return NotFound("Note not found");
                }

                if (model.IsApprove)
                {
                    existingNote.Suggested = false;
                }
                else
                {
                    await _noteManager.DeleteNoteAsync(model.Id);
                    return Ok("Note deleted");
                }
                
                var result = await _noteManager.UpdateNoteAsync(existingNote);
                return CreatedAtAction(nameof(GetNoteById), new { id = result.Id }, result);
            }

            return BadRequest("The user is not the owner");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("suggest")]
    [Authorize]
    public async Task<IActionResult> SuggestNote([FromBody] SuggestNote model)
    {
        if (model == null)
        {
            return BadRequest("Note model cannot be null.");
        }

        var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

        if (string.IsNullOrEmpty(token))
        {
            return BadRequest("Token cannot be null or empty.");
        }

        try
        {
            TokenHelpers.ValidateAndExtractUserId(token, _secretKey);

            var user = await _userManager.FindByIdAsync(model.UserId);
            var note = new Note()
            {
                Title = model.Title,
                Content = model.Content,
                CreatedAt = model.CreatedAt,
                AuthorId = model.UserId,
                Suggested = true,
                User = user
            };
            var result = await _noteManager.AddNoteAsync(note);

            return CreatedAtAction(nameof(GetNoteById), new { id = result.Id }, result);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost("delete-note")]
    [Authorize]
    public async Task<IActionResult> DeleteNote([FromBody] DeleteNote model)
    {
        if (model == null)
        {
            return BadRequest("Note model cannot be null.");
        }

        var token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

        if (string.IsNullOrEmpty(token))
        {
            return BadRequest("Token cannot be null or empty.");
        }

        try
        {
            var userId = TokenHelpers.ValidateAndExtractUserId(token, _secretKey);

            var user = await _userManager.FindByIdAsync(userId);
            if (!await _noteManager.IsUserNoteOwnerAsync(userId, model.Id))
                return BadRequest("The user is not the owner");

            var existingNote = await _noteManager.GetNoteByIdAsync(model.Id);

            if (existingNote == null)
            {
                return NotFound("Note not found");
            }

            var result = await _noteManager.DeleteNoteAsync(model.Id);
            return result ? Ok("Note was deleted") : BadRequest("Note was not deleted");
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetNoteById(int id)
    {
        try
        {
            var note = await _noteManager.GetNoteByIdAsync(id);

            if (note == null)
            {
                return NotFound($"Note with ID {id} not found.");
            }

            var result = new ShowNote()
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                CreatedAt = note.CreatedAt,
                IsPrivate = note.IsPrivate
            };

            return Ok(result);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500, "Internal server error. Please try again later.");
        }
    }
}