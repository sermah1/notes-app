﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IdentityServerApp.Migrations
{
    /// <inheritdoc />
    public partial class updatenote : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                schema: "identity",
                table: "Notes");

            migrationBuilder.RenameColumn(
                name: "UserId",
                schema: "identity",
                table: "Notes",
                newName: "AuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_UserId",
                schema: "identity",
                table: "Notes",
                newName: "IX_Notes_AuthorId");

            migrationBuilder.AddColumn<bool>(
                name: "Suggested",
                schema: "identity",
                table: "Notes",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_AuthorId",
                schema: "identity",
                table: "Notes",
                column: "AuthorId",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_AuthorId",
                schema: "identity",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "Suggested",
                schema: "identity",
                table: "Notes");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                schema: "identity",
                table: "Notes",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_AuthorId",
                schema: "identity",
                table: "Notes",
                newName: "IX_Notes_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                schema: "identity",
                table: "Notes",
                column: "UserId",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
