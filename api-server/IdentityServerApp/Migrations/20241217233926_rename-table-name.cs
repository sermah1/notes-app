﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IdentityServerApp.Migrations
{
    /// <inheritdoc />
    public partial class renametablename : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Note_AspNetUsers_UserId",
                schema: "identity",
                table: "Note");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Note",
                schema: "identity",
                table: "Note");

            migrationBuilder.RenameTable(
                name: "Note",
                schema: "identity",
                newName: "Notes",
                newSchema: "identity");

            migrationBuilder.RenameIndex(
                name: "IX_Note_UserId",
                schema: "identity",
                table: "Notes",
                newName: "IX_Notes_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notes",
                schema: "identity",
                table: "Notes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                schema: "identity",
                table: "Notes",
                column: "UserId",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                schema: "identity",
                table: "Notes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notes",
                schema: "identity",
                table: "Notes");

            migrationBuilder.RenameTable(
                name: "Notes",
                schema: "identity",
                newName: "Note",
                newSchema: "identity");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_UserId",
                schema: "identity",
                table: "Note",
                newName: "IX_Note_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Note",
                schema: "identity",
                table: "Note",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Note_AspNetUsers_UserId",
                schema: "identity",
                table: "Note",
                column: "UserId",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
