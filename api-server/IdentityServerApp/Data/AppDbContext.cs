﻿using IdentityServerApp.Models.Notes;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IdentityServerApp.Data;

public class AppDbContext : IdentityDbContext<IdentityUser>
{
    public AppDbContext(DbContextOptions options) : base(options)
    {
    }
    public DbSet<Note> Notes { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.HasDefaultSchema("identity");

        // Настройка связи между Note и IdentityUser
        modelBuilder.Entity<Note>()
            .ToTable("Notes")
            .HasOne(n => n.User) // Каждая заметка принадлежит одному пользователю
            .WithMany() // Один пользователь может иметь много заметок
            .HasForeignKey(n => n.AuthorId) // Указываем, что UserId является внешним ключом
            .OnDelete(DeleteBehavior.Cascade); // Удаление заметок при удалении пользователя
    }
}