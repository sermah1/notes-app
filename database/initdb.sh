#!/bin/sh
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE IF NOT EXISTS notes (
        id SERIAL PRIMARY KEY, 
        title VARCHAR(100) NOT NULL,
        content TEXT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );

    INSERT INTO notes (title, content) 
    VALUES 
        ('Заметка 1', 'Это первая заметка'), 
        ('Заметка 2', 'Это вторая заметка'), 
        ('Заметка 3', 'Это третья заметка');
EOSQL