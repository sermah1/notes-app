package com.sermah.todoshka.domain.model

data class Note(
    val id: Int,
    val title: String,
    val content: String,
    val createdAt: String,
    val public: Boolean,
    val suggest: Boolean,
    val ownerId: String,
)
