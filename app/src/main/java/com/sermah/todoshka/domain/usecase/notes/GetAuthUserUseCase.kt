package com.sermah.todoshka.domain.usecase.notes

import com.sermah.todoshka.domain.model.User

class GetAuthUserUseCase : UseCase() {

    fun execute(): User? =
        repository.getAuthUser()
}
