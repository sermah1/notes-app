package com.sermah.todoshka.domain.usecase.notes

import com.sermah.todoshka.NoteApp
import com.sermah.todoshka.domain.repository.Repository
import javax.inject.Inject

abstract class UseCase {
    @Inject
    lateinit var repository: Repository

    init {
        NoteApp.appComponent.inject(this)
    }
}
