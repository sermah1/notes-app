package com.sermah.todoshka.domain.model

data class NewNote(
    val title: String,
    val content: String,
    val public: Boolean,
)
