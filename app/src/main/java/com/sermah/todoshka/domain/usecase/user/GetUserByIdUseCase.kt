package com.sermah.todoshka.domain.usecase.user

import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.notes.UseCase

class GetUserByIdUseCase : UseCase() {

    suspend fun execute(id: String): User? =
        repository.getUser(id)
}
