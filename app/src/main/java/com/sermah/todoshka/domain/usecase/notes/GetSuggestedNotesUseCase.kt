package com.sermah.todoshka.domain.usecase.notes

import com.sermah.todoshka.domain.model.Note

class GetSuggestedNotesUseCase : UseCase() {

    suspend fun execute(): List<Note> =
        repository.getSuggestedNotes()
}
