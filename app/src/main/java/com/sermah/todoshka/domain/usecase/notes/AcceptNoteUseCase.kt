package com.sermah.todoshka.domain.usecase.notes

class AcceptNoteUseCase : UseCase() {

    suspend fun execute(noteId: Int) =
        repository.approveNote(noteId, true)
}
