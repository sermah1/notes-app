package com.sermah.todoshka.domain.usecase.notes

class DeleteNoteByIdUseCase : UseCase() {

    suspend fun execute(noteId: Int) =
        repository.deleteNote(noteId)
}
