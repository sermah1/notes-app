package com.sermah.todoshka.domain.usecase.notes

class SuggestNoteUseCase : UseCase() {

    suspend fun execute(title: String, body: String, to: String) =
        repository.suggestNote(title, body, to)
}
