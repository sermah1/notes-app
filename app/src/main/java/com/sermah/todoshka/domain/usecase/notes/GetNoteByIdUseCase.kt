package com.sermah.todoshka.domain.usecase.notes

import com.sermah.todoshka.domain.model.Note

class GetNoteByIdUseCase : UseCase() {

    suspend fun execute(noteId: Int): Note? =
        repository.getNote(noteId)
}
