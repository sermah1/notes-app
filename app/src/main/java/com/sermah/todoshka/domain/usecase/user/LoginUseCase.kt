package com.sermah.todoshka.domain.usecase.user

import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.usecase.notes.UseCase

class LoginUseCase : UseCase() {

    suspend fun execute(name: String, password: String): AuthInfo =
        repository.logIn(name, password)
}
