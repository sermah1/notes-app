package com.sermah.todoshka.domain.model

data class SuggestNote(
    val title: String,
    val content: String,
    val userId: Int,
)
