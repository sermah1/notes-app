package com.sermah.todoshka.domain.usecase.notes

import com.sermah.todoshka.domain.model.Note

class GetNotesByUserIdUseCase : UseCase() {

    suspend fun execute(userId: String): List<Note> =
        if (repository.getAuthUser()?.id == userId)
            repository.getNotes()
        else
            repository.getPublicNotes(userId)
}
