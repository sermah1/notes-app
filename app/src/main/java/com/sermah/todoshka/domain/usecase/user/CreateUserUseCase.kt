package com.sermah.todoshka.domain.usecase.user

import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.usecase.notes.UseCase

class CreateUserUseCase : UseCase() {
    suspend fun execute(name: String, password: String): AuthInfo =
        repository.createUser(name, password)
}
