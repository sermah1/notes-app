package com.sermah.todoshka.domain.usecase.notes

class DeclineNoteUseCase : UseCase() {

    suspend fun execute(noteId: Int) =
        repository.approveNote(noteId, false)
}
