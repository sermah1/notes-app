package com.sermah.todoshka.domain.usecase.notes

class CreateNoteUseCase : UseCase() {

    suspend fun execute(title: String, body: String, isPublic: Boolean) =
        repository.createNote(title, body, isPublic)
}
