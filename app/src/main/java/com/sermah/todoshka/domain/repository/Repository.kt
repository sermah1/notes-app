package com.sermah.todoshka.domain.repository

import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.model.User

interface Repository {
    suspend fun getNote(id: Int): Note?
    suspend fun getPublicNotes(userId: String): List<Note>
    suspend fun getSuggestedNotes(): List<Note>
    suspend fun getNotes(): List<Note>
    suspend fun createNote(title: String, content: String, isPublic: Boolean)
    suspend fun suggestNote(title: String, content: String, userId: String)
    suspend fun approveNote(noteId: Int, approve: Boolean)
    suspend fun editNote(id: Int, title: String, content: String, isPublic: Boolean)
    suspend fun deleteNote(id: Int)

    suspend fun getUserByName(name: String): User?
    suspend fun getUser(id: String): User?
    suspend fun logIn(name: String, password: String): AuthInfo
    suspend fun createUser(name: String, password: String): AuthInfo

    fun getAuthUser(): User?
    fun logOut(): Boolean
}
