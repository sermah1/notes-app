package com.sermah.todoshka.domain.model

data class AuthInfo(
    val isSuccess: Boolean,
    val user: User? = null,
    val token: String? = null,
    val error: String? = null,
)
