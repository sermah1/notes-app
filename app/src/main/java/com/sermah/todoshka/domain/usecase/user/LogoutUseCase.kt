package com.sermah.todoshka.domain.usecase.user

import com.sermah.todoshka.domain.usecase.notes.UseCase

class LogoutUseCase : UseCase() {

    fun execute(): Boolean =
        repository.logOut()
}
