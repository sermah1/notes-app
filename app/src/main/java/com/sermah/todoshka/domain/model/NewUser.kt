package com.sermah.todoshka.domain.model

data class NewUser(
    val name: String,
    val password: String,
)
