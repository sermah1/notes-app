package com.sermah.todoshka.domain.usecase.notes

class EditNoteByIdUseCase : UseCase() {

    suspend fun execute(noteId: Int, title: String, body: String, isPublic: Boolean) =
        repository.editNote(noteId, title, body, isPublic)
}
