package com.sermah.todoshka.domain.usecase.user

import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.notes.UseCase

class GetUserByNameUseCase : UseCase() {

    suspend fun execute(name: String): User? =
        repository.getUserByName(name)
}
