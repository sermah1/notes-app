package com.sermah.todoshka.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.user.GetUserByNameUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class UserFindViewModel : ViewModel() {
    private val _title = MutableStateFlow("")
    val title = _title.asStateFlow()

    fun find(name: String, onSuccess: (User) -> Unit, onFailure: (String) -> Unit) {
        viewModelScope.launch {
            val user = GetUserByNameUseCase().execute(name)

            launch(Dispatchers.Main) {
                if (user != null) {
                    onSuccess(user)
                } else {
                    onFailure("Not found")
                }
            }
        }
    }

    private companion object {
        const val TAG = "NoteListViewModel"
    }
}
