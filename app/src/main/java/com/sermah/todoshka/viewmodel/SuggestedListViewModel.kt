package com.sermah.todoshka.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.usecase.notes.AcceptNoteUseCase
import com.sermah.todoshka.domain.usecase.notes.DeclineNoteUseCase
import com.sermah.todoshka.domain.usecase.notes.GetSuggestedNotesUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SuggestedListViewModel : ViewModel() {
    private val _notesStateFlow: MutableStateFlow<List<Note>> = MutableStateFlow(listOf())
    val notesStateFlow = _notesStateFlow.asStateFlow()

    private val _title = MutableStateFlow("Suggested Posts")
    val title = _title.asStateFlow()

    fun declineNote(id: Int) {
        Log.d(TAG, "Declining note (id = $id)")
        viewModelScope.launch {
            DeclineNoteUseCase().execute(id)
            updateNotes()
        }
    }

    fun acceptNote(id: Int) {
        Log.d(TAG, "Accepting note (id = $id)")
        viewModelScope.launch {
            AcceptNoteUseCase().execute(id)
            updateNotes()
        }
    }

    fun updateNotes() {
        viewModelScope.launch {
            _notesStateFlow.value = GetSuggestedNotesUseCase().execute().also {
                Log.d(TAG, "Size = ${it.size}")
            }
        }
    }

    private companion object {
        const val TAG = "SuggestedListViewModel"
    }
}
