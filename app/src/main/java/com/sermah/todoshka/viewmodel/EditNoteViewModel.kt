package com.sermah.todoshka.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.notes.CreateNoteUseCase
import com.sermah.todoshka.domain.usecase.notes.EditNoteByIdUseCase
import com.sermah.todoshka.domain.usecase.notes.GetAuthUserUseCase
import com.sermah.todoshka.domain.usecase.notes.SuggestNoteUseCase
import com.sermah.todoshka.domain.usecase.user.GetUserByIdUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class EditNoteViewModel : ViewModel() {
    var noteId: Int = NO_ID
        private set
    var toUserId: String = ""
        private set

    private val _editType = MutableStateFlow(CREATE)
    val editType = _editType.asStateFlow()

    private val _title = MutableStateFlow("")
    val title = _title.asStateFlow()

    val authUser: User? get() = GetAuthUserUseCase().execute()

    fun submitNote(title: String, body: String, isPublic: Boolean) {
        Log.d(TAG, "submitNote id = $noteId (`$title`, `$body`, $isPublic)")
        when (editType.value) {
            SUGGEST_EDIT -> {
                Log.d(TAG, "Editing suggestion")
                viewModelScope.launch {
                    EditNoteByIdUseCase().execute(noteId, title, body, true)
                }
            }
            EDIT -> {
                Log.d(TAG, "Editing note")
                viewModelScope.launch {
                    EditNoteByIdUseCase().execute(noteId, title, body, isPublic)
                }
            }
            SUGGEST -> {
                Log.d(TAG, "Suggesting note")
                viewModelScope.launch {
                    SuggestNoteUseCase().execute(title, body, toUserId)
                }
            }
            else -> {
                Log.d(TAG, "Creating note")
                viewModelScope.launch {
                    CreateNoteUseCase().execute(title, body, isPublic)
                }
            }
        }
    }

    fun initialize(noteId: Int? = null, toUserId: String? = null) {
        this.noteId = noteId ?: NO_ID
        this.toUserId = toUserId ?: ""

        val isEdit = this.noteId != NO_ID
        val isSuggest = this.toUserId != "" && this.toUserId != authUser?.id

        _editType.value = when {
            !isEdit && isSuggest -> SUGGEST
            isEdit && !isSuggest -> EDIT
            isEdit && isSuggest -> SUGGEST_EDIT
            else -> CREATE
        }

        _editType.onEach { type ->
            when (type) {
                EDIT -> _title.value = "Edit note"
                CREATE -> _title.value = "New note"
                SUGGEST -> {
                    _title.value = "Suggest note"
                    GetUserByIdUseCase().execute(this.toUserId)?.let {
                        _title.value = "Suggest to ${it.name}"
                    }
                }
                SUGGEST_EDIT -> {
                    _title.value = "Edit note"
                    GetUserByIdUseCase().execute(this.toUserId)?.let {
                        _title.value = "Edit note to ${it.name}"
                    }
                }
            }
        }.launchIn(viewModelScope)
    }

    companion object {
        const val NO_ID = Int.MIN_VALUE
        const val CREATE = 0
        const val EDIT = 1
        const val SUGGEST = 2
        const val SUGGEST_EDIT = 3
        private const val TAG = "EditNoteViewModel"
    }
}
