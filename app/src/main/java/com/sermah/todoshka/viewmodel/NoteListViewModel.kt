package com.sermah.todoshka.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.notes.DeleteNoteByIdUseCase
import com.sermah.todoshka.domain.usecase.notes.GetAuthUserUseCase
import com.sermah.todoshka.domain.usecase.notes.GetNotesByUserIdUseCase
import com.sermah.todoshka.domain.usecase.user.GetUserByIdUseCase
import com.sermah.todoshka.domain.usecase.user.LogoutUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class NoteListViewModel : ViewModel() {
    private val _notesStateFlow: MutableStateFlow<List<Note>> = MutableStateFlow(listOf())
    val notesStateFlow = _notesStateFlow.asStateFlow()

    private val _title = MutableStateFlow("")
    val title = _title.asStateFlow()

    var viewUser: User? = null
        private set
    val authUser: User? get() = GetAuthUserUseCase().execute()

    fun deleteNote(id: Int) {
        Log.d(TAG, "Deleting note (id = $id)")
        viewModelScope.launch {
            DeleteNoteByIdUseCase().execute(id)
            updateNotes()
        }
    }

    fun updateNotes() = viewUser?.let { user ->
        viewModelScope.launch {
            _notesStateFlow.value = GetNotesByUserIdUseCase().execute(user.id)
        }
    }

    fun switchTo(id: String) {
        viewModelScope.launch {
            viewUser = GetUserByIdUseCase().execute(id)
            _title.value = viewUser?.name ?: "Notes"
            updateNotes()
        }
    }

    fun logOut() {
        LogoutUseCase().execute()
    }

    private companion object {
        const val TAG = "NoteListViewModel"
    }
}
