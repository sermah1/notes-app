package com.sermah.todoshka.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.usecase.notes.GetAuthUserUseCase
import com.sermah.todoshka.domain.usecase.user.CreateUserUseCase
import com.sermah.todoshka.domain.usecase.user.LoginUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    private val _isCreate = MutableStateFlow(false)
    val isCreate = _isCreate.asStateFlow()

    val title = isCreate
        .map { if (it) "Create User" else "Log In" }
        .stateIn(viewModelScope, SharingStarted.Eagerly, "")

    val authUser: User? get() = GetAuthUserUseCase().execute()

    fun login(name: String, pass: String, onResult: (AuthInfo) -> Unit) {
        viewModelScope.launch {
            val info = LoginUseCase().execute(name, pass)

            launch(Dispatchers.Main) {
                onResult(info)
            }
        }
    }

    fun createUser(name: String, pass: String, onResult: (AuthInfo) -> Unit) {
        viewModelScope.launch {
            val info = CreateUserUseCase().execute(name, pass)

            launch(Dispatchers.Main) {
                onResult(info)
            }
        }
    }

    fun switchMode() {
        _isCreate.update { !it }
    }
}
