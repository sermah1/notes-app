package com.sermah.todoshka.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

object ViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = when (modelClass) {
        EditNoteViewModel::class.java -> EditNoteViewModel()
        NoteListViewModel::class.java -> NoteListViewModel()
        SuggestedListViewModel::class.java -> SuggestedListViewModel()
        UserFindViewModel::class.java -> UserFindViewModel()
        LoginViewModel::class.java -> LoginViewModel()
        else -> super.create(modelClass)
    } as T
}
