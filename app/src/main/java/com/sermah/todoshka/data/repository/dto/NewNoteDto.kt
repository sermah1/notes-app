package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class NewNoteDto(
    @SerializedName("title")
    val title: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("isPrivate")
    val private: Boolean,
)
