package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class EditNoteDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("isPrivate")
    val private: Boolean,
)
