package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class FullUserDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("username")
    val name: String,
    @SerializedName("token")
    val token: String,
)
