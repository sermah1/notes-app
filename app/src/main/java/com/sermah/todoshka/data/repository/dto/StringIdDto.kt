package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class StringIdDto(
    @SerializedName("id")
    val id: String,
)
