package com.sermah.todoshka.data.repository

import com.sermah.todoshka.data.repository.dto.ApproveDto
import com.sermah.todoshka.data.repository.dto.EditNoteDto
import com.sermah.todoshka.data.repository.dto.FullUserDto
import com.sermah.todoshka.data.repository.dto.IdDto
import com.sermah.todoshka.data.repository.dto.LoginDto
import com.sermah.todoshka.data.repository.dto.NewNoteDto
import com.sermah.todoshka.data.repository.dto.NoteDto
import com.sermah.todoshka.data.repository.dto.RegisterDto
import com.sermah.todoshka.data.repository.dto.SuggestedNoteDto
import com.sermah.todoshka.data.repository.dto.UserDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    // --- Notes ---

    @GET("Note/{id}")
    suspend fun getNote(
        @Path("id") id: Int
    ): Response<NoteDto>

    @GET("Note/get-user-notes")
    suspend fun getUserNotes(): Response<List<NoteDto>>

    @GET("Note/get-public-user-notes/{id}")
    suspend fun getPublicNotes(
        @Path("id") id: String
    ): Response<List<NoteDto>>

    @GET("Note/get-suggested-user-notes")
    suspend fun getSuggestedNotes(): Response<List<NoteDto>>

    @POST("Note/add-note")
    suspend fun addNote(
        @Body note: NewNoteDto
    ): Response<Unit>

    @POST("Note/update-note")
    suspend fun updateNote(
        @Body note: EditNoteDto
    ): Response<Unit>

    @POST("Note/approve")
    suspend fun approveNote(
        @Body approve: ApproveDto
    ): Response<Unit>

    @POST("Note/suggest")
    suspend fun suggestNote(
        @Body suggest: SuggestedNoteDto
    ): Response<Unit>

    @POST("Note/delete-note")
    suspend fun deleteNote(
        @Body id: IdDto
    ): Response<Unit>

    // --- Account ---

    @POST("Account/register")
    suspend fun register(
        @Body register: RegisterDto
    ): Response<Unit>

    @POST("Account/login")
    suspend fun login(
        @Body login: LoginDto
    ): Response<FullUserDto>

    // --- User ---

    @GET("User/{id}")
    suspend fun getUser(
        @Path("id") id: String
    ): Response<UserDto>

    @GET("User/by-username/{username}")
    suspend fun getUserByName(
        @Path("username") name: String
    ): Response<UserDto>
}
