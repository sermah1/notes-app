package com.sermah.todoshka.data.repository.dto

import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.model.User

fun NoteDto.toNote(ownerId: String) = Note(
    id = this.id,
    title = this.title,
    content = this.content,
    createdAt = this.createdAt,
    public = !this.private,
    suggest = this.suggested,
    ownerId = ownerId,
)

fun UserDto.toUser() = User(
    id = this.id,
    name = this.name,
)
