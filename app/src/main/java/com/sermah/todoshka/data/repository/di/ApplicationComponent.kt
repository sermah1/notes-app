package com.sermah.todoshka.data.repository.di

import com.sermah.todoshka.domain.usecase.notes.UseCase
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MainModule::class])
interface ApplicationComponent {
    fun inject(useCase: UseCase)
}
