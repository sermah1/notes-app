package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class ApproveDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("isApprove")
    val approve: Boolean,
)
