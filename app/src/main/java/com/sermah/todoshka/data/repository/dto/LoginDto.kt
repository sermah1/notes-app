package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class LoginDto(
    @SerializedName("username")
    val name: String,
    @SerializedName("password")
    val password: String,
)
