package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class IdDto(
    @SerializedName("id")
    val id: Int,
)
