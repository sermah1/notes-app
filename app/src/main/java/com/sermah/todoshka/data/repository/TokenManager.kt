package com.sermah.todoshka.data.repository

object TokenManager {
    private var token: String? = null

    fun getToken(): String? = token
    fun hasToken(): Boolean = token != null

    fun setToken(token: String?) {
        this.token = token
    }
}
