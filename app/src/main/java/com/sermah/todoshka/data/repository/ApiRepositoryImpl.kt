package com.sermah.todoshka.data.repository

import android.util.Log
import com.sermah.todoshka.data.repository.dto.ApproveDto
import com.sermah.todoshka.data.repository.dto.EditNoteDto
import com.sermah.todoshka.data.repository.dto.IdDto
import com.sermah.todoshka.data.repository.dto.LoginDto
import com.sermah.todoshka.data.repository.dto.NewNoteDto
import com.sermah.todoshka.data.repository.dto.RegisterDto
import com.sermah.todoshka.data.repository.dto.SuggestedNoteDto
import com.sermah.todoshka.data.repository.dto.toNote
import com.sermah.todoshka.data.repository.dto.toUser
import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.repository.Repository
import retrofit2.Response
import javax.inject.Inject

class ApiRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : Repository {
    private var authInfo: AuthInfo? = null
        set(value) {
            field = value
            Log.d(TAG, "New info $field")
            TokenManager.setToken(value?.token)
        }

    override suspend fun getNote(id: Int): Note? =
        doReq {
            Log.d(TAG, "getNote($id)")
            apiService.getNote(id)
        }?.toNote("")

    override suspend fun getPublicNotes(userId: String): List<Note> =
        doReq {
            Log.d(TAG, "getPublicNotes($userId)")
            apiService.getPublicNotes(userId)
        }?.map { it.toNote(userId) } ?: emptyList()

    override suspend fun createNote(title: String, content: String, isPublic: Boolean) {
        doReq {
            Log.d(TAG, "createNote(...)")
            apiService.addNote(NewNoteDto(title, content, !isPublic))
        }
    }

    override suspend fun editNote(id: Int, title: String, content: String, isPublic: Boolean) {
        doReq {
            Log.d(TAG, "editNote($id)")
            apiService.updateNote(EditNoteDto(id, title, content, !isPublic))
        }
    }

    override suspend fun deleteNote(id: Int) {
        doReq {
            Log.d(TAG, "deleteNote($id)")
            apiService.deleteNote(IdDto(id))
        }
    }

    override suspend fun suggestNote(title: String, content: String, userId: String) {
        doReq {
            Log.d(TAG, "suggestNote(uid = $userId)")
            apiService.suggestNote(SuggestedNoteDto(title, content, userId))
        }
    }

    override suspend fun getUser(id: String): User? =
        doReq {
            Log.d(TAG, "getUser($id)")
            apiService.getUser(id)
        }?.toUser()

    override suspend fun createUser(name: String, password: String): AuthInfo =
        apiService.register(RegisterDto(name, password, "")).let { reg ->
            if (!reg.isSuccessful)
                return@let AuthInfo(false, error = reg.message())

            logIn(name, password)
        }

    override suspend fun getUserByName(name: String): User? =
        doReq {
            Log.d(TAG, "getUserByName($name)")
            apiService.getUserByName(name)
        }?.toUser()

    override suspend fun approveNote(noteId: Int, approve: Boolean) {
        doReq {
            Log.d(TAG, "approveNote($noteId, $approve)")
            apiService.approveNote(ApproveDto(noteId, approve))
        }
    }

    override suspend fun logIn(name: String, password: String): AuthInfo {
        val resp = apiService.login(LoginDto(name, password))
        return if (resp.isSuccessful) {
            val login = resp.body() ?: run {
                Log.e(TAG, "Failed to parse body of login")
                return@logIn AuthInfo(true)
            }

            AuthInfo(true, User(login.id, login.name), login.token).also {
                authInfo = it
            }
        } else {
            logRespError(resp)
            AuthInfo(false, error = resp.message())
        }
    }

    override fun getAuthUser(): User? = authInfo?.user

    override fun logOut(): Boolean {
        val ok = authInfo != null
        authInfo = null
        return ok
    }

    override suspend fun getNotes(): List<Note> =
        doReq {
            Log.d(TAG, "getNotes()")
            apiService.getUserNotes()
        }?.filter { !it.suggested }
            ?.map { it.toNote(authInfo?.user?.id ?: "") }
            ?: emptyList()

    override suspend fun getSuggestedNotes(): List<Note> =
        doReq {
            Log.d(TAG, "getSuggestedNotes()")
            apiService.getSuggestedNotes()
        }?.map { it.toNote(authInfo?.user?.id ?: "") }
            ?: emptyList()

    private suspend fun <T> doReq(req: suspend () -> Response<T>): T? {
        val resp = req()
        if (resp.isSuccessful) {
            return resp.body() ?: run {
                Log.e("ApiRepositoryImpl", "Failed to parse body of $req")
                null
            }
        } else {
            logRespError(resp)
            return null
        }
    }

    private fun logRespError(resp: Response<*>) {
        Log.e(TAG, resp.message())
    }

    companion object {
        private const val TAG = "ApiRepositoryImpl"
    }
}
