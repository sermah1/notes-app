package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class NoteDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("isPrivate")
    val private: Boolean,
    @SerializedName("isSuggest")
    val suggested: Boolean,
)
