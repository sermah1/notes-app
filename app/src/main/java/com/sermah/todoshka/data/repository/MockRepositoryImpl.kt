package com.sermah.todoshka.data.repository

import android.util.Log
import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.domain.repository.Repository
import java.util.Locale
import javax.inject.Singleton

@Singleton
object MockRepositoryImpl : Repository {

    private var noteCounter = 0
    private var userCounter = 0
    private val authUserId
        get() = successfulAuth?.user?.id ?: ""

    private var successfulAuth: AuthInfo? = null

    private val notes: MutableMap<Int, NoteData> = listOf(
        newNote(
            "Test item",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            true,
            "0",
        ),
        newNote(
            "Diam maecenas sed enim ut sem viverra aliquet.",
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            true,
            "0",
        ),
        newNote(
            "Private note.",
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            false,
            "0",
        ),
        newNote(
            "Private note 2",
            "What's really exciting is that we're enabling teams to deploy AI systems that would otherwise be too risky. We let you focus on building powerful agents while knowing that critical steps will always get a human-in-the-loop. It's been dope seeing people start to think bigger when they consider dynamic human oversight as a key ingredient in production AI systems",
            false,
            "0",
        ),
        newNote(
            "Note by us1",
            "What's really exciting is that we're enabling teams to deploy AI systems that would otherwise be too risky. We let you focus on building powerful agents while knowing that critical steps will always get a human-in-the-loop. It's been dope seeing people start to think bigger when they consider dynamic human oversight as a key ingredient in production AI systems",
            true,
            "1",
        ),
        newNote(
            "Note by us1 by 2",
            "What's really exciting is that we're enabling teams to deploy AI systems that would otherwise be too risky. We let you focus on building powerful agents while knowing that critical steps will always get a human-in-the-loop. It's been dope seeing people start to think bigger when they consider dynamic human oversight as a key ingredient in production AI systems",
            true,
            "1",
            true,
        ),
        newNote(
            "Note by us1 by admin",
            "admin What's really exciting is that we're enabling teams to deploy AI systems that would otherwise be too risky. We let you focus on building powerful agents while knowing that critical steps will always get a human-in-the-loop. It's been dope seeing people start to think bigger when they consider dynamic human oversight as a key ingredient in production AI systems",
            true,
            "1",
            true,
        ),
        newNote(
            "Private Note by us1",
            "SECRETS What's really exciting is that we're enabling teams to deploy AI systems that would otherwise be too risky. We let you focus on building powerful agents while knowing that critical steps will always get a human-in-the-loop. It's been dope seeing people start ",
            false,
            "1",
        ),
        newNote(
            "Suggested to us1 by adm",
            "SUGGESTEETD  111 It's been dope seeing people start ",
            true,
            "1",
            true,
        ),
        newNote(
            "Suggested to adm by us2",
            "SUGGESTEETD  222 It's been dope seeing people start ",
            true,
            "0",
            true,
        ),
    ).associateBy { it.id }.toMutableMap()

    private val users: MutableMap<String, UserData> = listOf(
        newUser("admin", "admin"),
        newUser("us1", "1111"),
        newUser("us2", "2222"),
    ).associateBy { it.id }.toMutableMap()

    override suspend fun getPublicNotes(userId: String): List<Note> {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        return notes.values
            .filter { (it.ownerId == userId) && !it.suggested && it.public }
            .map { it.toNote() }
    }

    override suspend fun getNotes(): List<Note> {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        return notes.values
            .filter { (it.ownerId == authUserId) && !it.suggested }
            .map { it.toNote() }
    }

    override suspend fun getSuggestedNotes(): List<Note> {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        return notes.values
            .filter { (it.ownerId == authUserId) && it.suggested }
            .map { it.toNote() }
    }

    override suspend fun createNote(title: String, content: String, isPublic: Boolean) {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        val note = newNote(title, content, isPublic)
        notes[note.id] = note
    }

    override suspend fun editNote(id: Int, title: String, content: String, isPublic: Boolean) {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        val note = notes[id] ?: return
        notes[note.id] = note.copy(title = title, content = content, public = isPublic)
    }

    override suspend fun getNote(id: Int): Note? {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        val note = notes[id] ?: throw Exception("Note not found")
        if (!note.public && note.ownerId != getAuthUser()?.id)
            throw Exception("Forbidden")

        return note.toNote()
    }

    override suspend fun suggestNote(title: String, content: String, userId: String) {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        val note = newNote(title, content, true, ownerId = userId, suggested = true)
        notes[note.id] = note
    }

    override suspend fun approveNote(id: Int, approve: Boolean) {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        val note = notes[id]
        if (note?.ownerId != authUserId)
            throw Exception("Forbidden")

        if (approve)
            notes[id] = newNote(
                note.title,
                note.content,
                note.public,
                note.ownerId,
                false,
            )
        else
            notes.remove(id)
    }

    override suspend fun deleteNote(id: Int) {
        if (authUserId.isEmpty())
            throw Exception("Unauthorized")
        Log.d(TAG, "deleteNote($id)")
        if (notes[id]?.ownerId == authUserId)
            notes.remove(id)
        else
            throw Exception("Forbidden")
    }

    override suspend fun createUser(name: String, password: String): AuthInfo {
        val name = name.lowercase(Locale.ROOT)
        val password = password.lowercase(Locale.ROOT)

        if (users.any { it.value.name == name })
            return AuthInfo(false, error = "User exists")

        val id = userCounter
        val ud = newUser(name, password)
        users["$id"] = ud
        return AuthInfo(true, User(ud.id, ud.name), ud.token).also {
            successfulAuth = it
        }
    }

    override suspend fun getUser(id: String): User? = users[id]?.toUser()

    override suspend fun getUserByName(name: String): User? = users.values
        .find { it.name == name }
        ?.toUser()

    override suspend fun logIn(name: String, password: String): AuthInfo {
        val name = name.lowercase()
        val password = password.lowercase()
        val u = users.values
            .find { it.name == name && it.password == password }
            ?: return AuthInfo(false, error = "No matching users")

        return AuthInfo(true, u.toUser(), u.token).also {
            successfulAuth = it
        }
    }

    override fun getAuthUser(): User? = successfulAuth?.user

    override fun logOut(): Boolean {
        val ok = successfulAuth != null
        successfulAuth = null
        return ok
    }

    private fun newNote(title: String, content: String, isPublic: Boolean, ownerId: String = authUserId, suggested: Boolean = false) = NoteData(
        noteCounter++,
        title,
        content,
        "",
        isPublic,
        suggested,
        ownerId
    )

    private fun newUser(name: String, pass: String) = UserData(
        "${userCounter++}",
        name.lowercase(),
        pass.lowercase(),
        "token$name$pass",
    )

    private const val TAG = "MockNoteRepo"

    private data class NoteData(
        val id: Int,
        val title: String,
        val content: String,
        val createdAt: String,
        val public: Boolean,
        val suggested: Boolean,
        val ownerId: String,
    )

    private fun NoteData.toNote() = Note(
        this.id, this.title, this.content, this.createdAt, this.public, this.suggested, this.ownerId
    )

    private data class UserData(
        val id: String,
        val name: String,
        val password: String,
        val token: String,
    )

    private fun UserData.toUser() = User(
        this.id, this.name
    )
}
