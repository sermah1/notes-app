package com.sermah.todoshka.data.repository.dto

import com.google.gson.annotations.SerializedName

data class SuggestedNoteDto(
    @SerializedName("title")
    val title: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("userId")
    val userId: String,
)
