package com.sermah.todoshka.data.repository.di

import com.sermah.todoshka.data.repository.ApiRepositoryImpl
import com.sermah.todoshka.data.repository.ApiService
import com.sermah.todoshka.data.repository.RetrofitBuilder
import com.sermah.todoshka.domain.repository.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule {
    @Provides
    fun provideApiService(): ApiService =
        RetrofitBuilder.apiService

    @Provides
    @Singleton
    fun provideRepository(apiService: ApiService): Repository =
        ApiRepositoryImpl(apiService)
//        MockRepositoryImpl
}
