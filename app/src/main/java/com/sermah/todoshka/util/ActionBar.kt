package com.sermah.todoshka.util

import androidx.fragment.app.Fragment
import com.sermah.todoshka.MainActivity

fun Fragment.setTitle(title: String) =
    (activity as? MainActivity)?.setActionBarTitle(title)
