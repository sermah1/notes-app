package com.sermah.todoshka.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.sermah.todoshka.R
import com.sermah.todoshka.databinding.FragmentLoginBinding
import com.sermah.todoshka.domain.model.AuthInfo
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.util.setInOutAnimations
import com.sermah.todoshka.util.setTitle
import com.sermah.todoshka.viewmodel.LoginViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class LoginFragment : Fragment() {
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val authed = viewModel.authUser
        if (authed != null) {
            Log.d("LoginFragment", "Already logged in")
            showListOf(authed)
        }

        binding.apply {
            loginButton.setOnClickListener {
                val name = loginName.text.toString().lowercase()
                val pass = loginPass.text.toString().lowercase()
                val confirm = loginConfirm.text.toString().lowercase()

                if (viewModel.isCreate.value && pass != confirm) {
                    loginMessage.text = "Passwords do not match"
                    return@setOnClickListener
                } else if (pass.isEmpty() || name.isEmpty()) {
                    loginMessage.text = "Values can't be empty"
                    return@setOnClickListener
                }

                loginMessage.text = ""

                if (viewModel.isCreate.value) {
                    viewModel.createUser(name, pass, onResult = ::onCreateResult)
                } else {
                    viewModel.login(name, pass, onResult = ::onLoginResult)
                }
            }

            loginAltButton.setOnClickListener {
                viewModel.switchMode()
            }
        }

        viewModel.title
            .onEach {
                setTitle(it)
            }
            .launchIn(lifecycleScope)

        viewModel.isCreate
            .onEach { create ->
                binding.apply {
                    loginMessage.text = ""
                    if (create) {
                        loginConfirm.visibility = View.VISIBLE
                        loginButton.text = getString(R.string.btn_create)
                        loginAltButton.text = getString(R.string.btn_login)
                    } else {
                        loginConfirm.visibility = View.GONE
                        loginButton.text = getString(R.string.btn_login)
                        loginAltButton.text = getString(R.string.btn_create)
                    }
                }
            }
            .launchIn(lifecycleScope)
    }

    private fun onLoginResult(info: AuthInfo) {
        with(binding) {
            if (info.isSuccess && info.user != null) {
                showListOf(info.user)
                Log.d("LoginFragment", "Logged in")
            } else if (info.isSuccess) {
                loginMessage.text = "Login succeeded, but didn't receive User instance"
            } else {
                loginMessage.text = "Login failed\n" +
                    "${info.error}"
            }
        }
    }

    private fun onCreateResult(info: AuthInfo) {
        with(binding) {
            if (info.isSuccess && info.user != null) {
                showListOf(info.user)
            } else if (info.isSuccess) {
                loginMessage.text = "User created, but didn't receive User instance"
            } else {
                loginMessage.text = "Creation failed\n${info.error}"
            }
        }
    }

    private fun showListOf(user: User) {
        parentFragmentManager.commit {
            setInOutAnimations()
            replace(
                R.id.fragment_container, NoteListFragment::class.java,
                Bundle().apply {
                    putString("user", user.id)
                }
            )
            parentFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    override fun onResume() {
        super.onResume()
        setTitle(viewModel.title.value)
    }
}
