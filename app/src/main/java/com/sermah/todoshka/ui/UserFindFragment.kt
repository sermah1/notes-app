package com.sermah.todoshka.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import com.sermah.todoshka.R
import com.sermah.todoshka.databinding.FragmentFindUserBinding
import com.sermah.todoshka.domain.model.User
import com.sermah.todoshka.util.setInOutAnimations
import com.sermah.todoshka.util.setTitle
import com.sermah.todoshka.viewmodel.UserFindViewModel

class UserFindFragment : Fragment() {
    private val viewModel: UserFindViewModel by viewModels()
    private lateinit var binding: FragmentFindUserBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFindUserBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            findButton.setOnClickListener {
                val name = findName.text.toString().lowercase()
                viewModel.find(name, onSuccess = { u ->
                    binding.findMessage.text = ""
                    showListOf(u)
                }, onFailure = { msg ->
                    binding.findMessage.text = msg
                })
            }
        }

        setTitle(getString(R.string.title_find_user))
    }

    private fun showListOf(user: User) {
        parentFragmentManager.commit {
            setInOutAnimations()
            replace(
                R.id.fragment_container, NoteListFragment::class.java,
                Bundle().apply {
                    putString("user", user.id)
                }
            )
            parentFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    override fun onResume() {
        super.onResume()
        setTitle(getString(R.string.title_find_user))
    }
}
