package com.sermah.todoshka.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.sermah.todoshka.R
import com.sermah.todoshka.databinding.FragmentCreateNoteBinding
import com.sermah.todoshka.util.setTitle
import com.sermah.todoshka.viewmodel.EditNoteViewModel
import com.sermah.todoshka.viewmodel.EditNoteViewModel.Companion.CREATE
import com.sermah.todoshka.viewmodel.EditNoteViewModel.Companion.EDIT
import com.sermah.todoshka.viewmodel.EditNoteViewModel.Companion.SUGGEST
import com.sermah.todoshka.viewmodel.EditNoteViewModel.Companion.SUGGEST_EDIT
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class EditNoteFragment : Fragment() {

    private val viewModel: EditNoteViewModel by viewModels()
    private lateinit var binding: FragmentCreateNoteBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateNoteBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setTitle(viewModel.title.value)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            val noteId = args.getInt("id", EditNoteViewModel.NO_ID)
            val toUserId = args.getString("to", "")
            with(binding) {
                editTitle.setText(args.getString("title", ""))
                editBody.setText(args.getString("body", ""))
                editPublic.isChecked = args.getBoolean("public", false)
            }

            viewModel.initialize(noteId, toUserId)
        }

        binding.apply {
            editSubmit.setOnClickListener {
                viewModel.submitNote(
                    binding.editTitle.text.toString().trim(),
                    binding.editBody.text.toString().trim(),
                    binding.editPublic.isChecked
                )

                parentFragmentManager.popBackStack()
            }
        }

        viewModel.editType.onEach { type ->
            binding.apply {
                when (type) {
                    CREATE -> {
                        editSubmit.text = resources.getText(R.string.btn_create)
                        editPublic.visibility = View.VISIBLE
                    }
                    EDIT -> {
                        editSubmit.text = resources.getText(R.string.btn_edit)
                        editPublic.visibility = View.VISIBLE
                    }
                    SUGGEST -> {
                        editSubmit.text = resources.getText(R.string.btn_suggest)
                        editPublic.visibility = View.GONE
                    }
                    SUGGEST_EDIT -> {
                        editSubmit.text = resources.getText(R.string.btn_edit)
                        editPublic.visibility = View.GONE
                    }
                }
            }
        }.launchIn(lifecycleScope)

        viewModel.title.onEach { newTitle ->
            setTitle(newTitle)
        }.launchIn(lifecycleScope)
    }
}
