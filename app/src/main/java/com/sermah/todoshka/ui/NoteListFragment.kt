package com.sermah.todoshka.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.DividerItemDecoration
import com.sermah.todoshka.R
import com.sermah.todoshka.databinding.FragmentNoteListBinding
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.ui.adapter.NoteListAdapter
import com.sermah.todoshka.util.setInOutAnimations
import com.sermah.todoshka.util.setTitle
import com.sermah.todoshka.viewmodel.NoteListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class NoteListFragment : Fragment(), MenuProvider {

    private val viewModel: NoteListViewModel by viewModels()
    private lateinit var binding: FragmentNoteListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNoteListBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val authed = viewModel.authUser
        if (authed == null) {
            Log.d(TAG, "Not logged in")
            showLogin()
            return
        }

        with(binding) {
            notesRecycler.adapter = NoteListAdapter(emptyList(), authed.id, ::onItemMenuClick)
            notesRecycler.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            addNoteFab.setOnClickListener {
                parentFragmentManager.commit {
                    setInOutAnimations()
                    val isSuggest = !isViewingAuthUser()
                    val viewId = viewModel.viewUser?.id
                    replace(
                        R.id.fragment_container, EditNoteFragment::class.java,
                        Bundle().also { b ->
                            if (isSuggest) viewId?.let {
                                b.putString("to", it)
                            }
                        }
                    )
                    addToBackStack(if (isSuggest) "suggest_$viewId" else "create")
                }
            }
        }

        lifecycleScope.launch {
            viewModel.notesStateFlow.collectLatest {
                (binding.notesRecycler.adapter as NoteListAdapter).update(it)
                Log.d(TAG, "Updated size = ${it.size}")
            }
        }

        requireActivity()
            .addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)

        viewModel.title.onEach { newTitle ->
            viewModel.viewModelScope.launch(Dispatchers.Main) {
                setTitle(newTitle)
            }
        }.launchIn(lifecycleScope)

        arguments?.let { args ->
            val userId = args.getString("user", "")

            viewModel.switchTo(userId)
        }
    }

    override fun onResume() {
        super.onResume()
        setTitle(viewModel.title.value)
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.action_bar, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.action_refresh -> {
                viewModel.updateNotes()
            }
            R.id.action_find_user -> {
                parentFragmentManager.commit {
                    setInOutAnimations()
                    replace(
                        R.id.fragment_container, UserFindFragment()
                    )
                    addToBackStack("find_user")
                }
            }
            R.id.action_suggested -> {
                parentFragmentManager.commit {
                    setInOutAnimations()
                    replace(
                        R.id.fragment_container, SuggestedListFragment()
                    )
                    addToBackStack("suggests")
                }
            }
            R.id.action_logout -> {
                viewModel.logOut()
                parentFragmentManager.commit {
                    setInOutAnimations()
                    replace(
                        R.id.fragment_container, LoginFragment()
                    )
                    addToBackStack("login")
                }
            }
        }
        return true
    }

    private fun showLogin() {
        parentFragmentManager.commit {
            setInOutAnimations()
            replace(
                R.id.fragment_container, LoginFragment()
            )
            parentFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    private fun isViewingAuthUser() = viewModel.viewUser?.id == viewModel.authUser?.id

    private fun onItemMenuClick(menuItem: MenuItem, note: Note) {
        when (val id = menuItem.itemId) {
            R.id.menu_edit -> {
                Log.d(TAG, "Edit note (id = ${note.id})")
                parentFragmentManager.commit {
                    setInOutAnimations()
                    replace(
                        R.id.fragment_container, EditNoteFragment::class.java,
                        Bundle().also { b ->
                            b.putString("title", note.title)
                            b.putString("body", note.content)
                            b.putBoolean("public", note.public)
                            b.putInt("id", note.id)
                            b.putString("to", note.ownerId)
                        }
                    )
                    addToBackStack("edit_${note.id}")
                }
            }
            R.id.menu_delete -> {
                Log.d(TAG, "Delete note (id = ${note.id})")
                viewModel.deleteNote(note.id)
            }
            else -> {
                Log.d(TAG, "Unknown menuItem id - ${resources.getResourceName(id)}")
            }
        }
    }

    private companion object {
        const val TAG = "NoteListFragment"
    }
}
