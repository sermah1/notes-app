package com.sermah.todoshka.ui.adapter

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.sermah.todoshka.R
import com.sermah.todoshka.databinding.ListItemBinding
import com.sermah.todoshka.domain.model.Note

class NoteListAdapter(
    startItems: List<Note>,
    private val authId: String,
    private val onMenuItemClick: (menuItem: MenuItem, note: Note) -> Unit,
) : RecyclerView.Adapter<NoteListAdapter.NoteItemViewHolder>() {
    private val items = startItems.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemBinding.inflate(inflater, parent, false)
        return NoteItemViewHolder(binding, onMenuItemClick)
    }

    override fun onBindViewHolder(holder: NoteItemViewHolder, position: Int) {
        holder.note = items[position]
        holder.allowEdit = items[position].ownerId == authId
    }

    fun update(newItems: List<Note>) {
        val result = DiffUtil.calculateDiff(
            ItemsDiffUtilCallback(
                items, newItems
            )
        )
        items.clear()
        items.addAll(newItems)
        result.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = items.size

    class NoteItemViewHolder(
        private val binding: ListItemBinding,
        val onMenuItemClick: (menuItem: MenuItem, note: Note) -> Unit,
    ) : ViewHolder(binding.root) {
        var allowEdit: Boolean = false
            set(value) {
                field = value
                if (value) {
                    binding.root.setOnCreateContextMenuListener(onCreateContextMenuListener)
                } else {
                    binding.root.setOnCreateContextMenuListener(null)
                }
            }

        private val onCreateContextMenuListener = View.OnCreateContextMenuListener { menu, _, _ ->
            menu.add(0, R.id.menu_edit, 0, R.string.menu_edit)
                .setOnMenuItemClickListener(::handleMenuItemClick)
            menu.add(0, R.id.menu_delete, 1, R.string.menu_delete)
                .setOnMenuItemClickListener(::handleMenuItemClick)
        }

        var note: Note? = null
            set(value) {
                field = value?.also {
                    binding.noteTitle.text = it.title
                    binding.noteTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, if (it.public) 0 else R.drawable.ic_lock, 0)
                    binding.noteBody.text = it.content
                }
            }

        private fun handleMenuItemClick(menuItem: MenuItem): Boolean {
            note?.let { onMenuItemClick(menuItem, it) }
            return true
        }
    }

    class ItemsDiffUtilCallback(
        private val oldList: List<Note>,
        private val newList: List<Note>,
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] === newList[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size
    }
}
