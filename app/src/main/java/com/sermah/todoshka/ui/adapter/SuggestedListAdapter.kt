package com.sermah.todoshka.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.sermah.todoshka.databinding.ListSuggestItemBinding
import com.sermah.todoshka.domain.model.Note

class SuggestedListAdapter(
    startItems: List<Note>,
    private val onAccept: (note: Note) -> Unit,
    private val onDecline: (note: Note) -> Unit,
) : RecyclerView.Adapter<SuggestedListAdapter.SuggestedNoteItemViewHolder>() {
    private val items = startItems.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestedNoteItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListSuggestItemBinding.inflate(inflater, parent, false)
        return SuggestedNoteItemViewHolder(binding, onAccept, onDecline)
    }

    override fun onBindViewHolder(holder: SuggestedNoteItemViewHolder, position: Int) {
        holder.note = items[position]
    }

    fun update(newItems: List<Note>) {
        val result = DiffUtil.calculateDiff(
            ItemsDiffUtilCallback(
                items, newItems
            )
        )
        items.clear()
        items.addAll(newItems)
        result.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = items.size

    class SuggestedNoteItemViewHolder(
        private val binding: ListSuggestItemBinding,
        private val onAccept: (note: Note) -> Unit,
        private val onDecline: (note: Note) -> Unit,
    ) : ViewHolder(binding.root) {
        var note: Note? = null
            set(value) {
                field = value?.also {
                    binding.noteTitle.text = it.title
                    binding.noteBody.text = it.content
                }
            }

        init {
            binding.apply {
                suggestAccept.setOnClickListener {
                    note?.let {
                        onAccept(it)
                    }
                }
                suggestDecline.setOnClickListener {
                    note?.let {
                        onDecline(it)
                    }
                }
            }
        }
    }

    class ItemsDiffUtilCallback(
        private val oldList: List<Note>,
        private val newList: List<Note>,
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] === newList[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size
    }
}
