package com.sermah.todoshka.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import com.sermah.todoshka.databinding.FragmentSuggestedListBinding
import com.sermah.todoshka.domain.model.Note
import com.sermah.todoshka.ui.adapter.SuggestedListAdapter
import com.sermah.todoshka.util.setTitle
import com.sermah.todoshka.viewmodel.SuggestedListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class SuggestedListFragment : Fragment() {

    private val viewModel: SuggestedListViewModel by viewModels()
    lateinit var binding: FragmentSuggestedListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSuggestedListBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            suggestedRecycler.adapter = SuggestedListAdapter(emptyList(), ::onAccept, ::onDecline)
            suggestedRecycler.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
        }

        lifecycleScope.launch {
            viewModel.notesStateFlow.collectLatest {
                (binding.suggestedRecycler.adapter as SuggestedListAdapter).update(it)
                Log.d(TAG, "Updated size = ${it.size}")
            }
        }

        viewModel.title.onEach { newTitle ->
            lifecycleScope.launch(Dispatchers.Main) {
                setTitle(newTitle)
            }
        }.launchIn(lifecycleScope)

        viewModel.updateNotes()
    }

    override fun onResume() {
        super.onResume()
        setTitle(viewModel.title.value)
    }

    private fun onAccept(note: Note) {
        viewModel.acceptNote(note.id)
    }

    private fun onDecline(note: Note) {
        viewModel.declineNote(note.id)
    }

    private companion object {
        const val TAG = "SuggestedListFragment"
    }
}
