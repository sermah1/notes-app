package com.sermah.todoshka

import android.app.Application
import com.sermah.todoshka.data.repository.di.DaggerApplicationComponent

class NoteApp : Application() {
    companion object {
        val appComponent = DaggerApplicationComponent.create()
    }
}
